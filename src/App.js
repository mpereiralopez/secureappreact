// @flow
import React from 'react';
import { StackNavigator } from 'react-navigation';
import { Root } from 'native-base';
import Auth from './container/AuthContainer';
import Home from './container/HomeContainer';
import Intro from './container/IntroContainer';
import Events from './container/EventContainer';
import EventTypesList from './container/BlankPageContainer';
// import Sidebar from './container/SidebarContainer';

/* const Drawer = DrawerNavigator(
   {
	   Home: { screen: Home },
   },
   {
	   initialRouteName: 'Home',
	   contentComponent: props => <Sidebar {...props} />,
   }
); */

const App = StackNavigator(
  {
    Intro: { screen: Intro },
    Auth: { screen: Auth },
    EventTypesList: { screen: EventTypesList },
    Events: { screen: Events },
    Home: { screen: Home },
  },
  {
    initialRouteName: 'Intro',
    headerMode: 'none',
  },
);

const prevGetStateForAction = App.router.getStateForAction;

App.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Home
  if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'Home') {
    return null;
  }
  // Do not allow to go back to Login
  if (action.type === 'Navigation/BACK' && state) {
    const newRoutes = state.routes.filter(r => r.routeName !== 'Auth');
    const newIndex = newRoutes.length - 1;
    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }
  return prevGetStateForAction(action, state);
};

export default () => (
  <Root>
    <App />
  </Root>
);
