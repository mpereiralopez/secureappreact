import Service from './../services';

export default class eventTypesActions {
  constructor() {
    this.service = new Service();
  }

  async getEventTypeList() {
    const payload = await this.service.getEventTypeList();
    return payload;
  }
}
