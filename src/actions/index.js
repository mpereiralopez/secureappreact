import UserActions from './user';
import EventTypesActions from './eventTypes';

module.exports = {
  UserActions,
  EventTypesActions,
};
