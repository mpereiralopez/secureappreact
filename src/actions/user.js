import Service from './../services';

const listIsLoading = (bool: boolean) => (
  {
    type: 'LIST_IS_LOADING',
    isLoading: bool,
  });

const retreiveUserData = (userData: Object, error: Object) => (
  {
    type: 'RETREIVE_USER_DATA',
    userData,
    error,
  });

export default class userActions {
  constructor() {
    this.service = new Service();
  }
  getUserFromDB() {
    return async (dispatch) => {
      dispatch(listIsLoading(true));
      const { userInfo, error } = await this.service.getUser();
      dispatch(retreiveUserData(userInfo, error));
    };
  }

  async registerUser(userInfo) {
    const payload = await this.service.registerUser(userInfo);
    return payload;
  }
}
