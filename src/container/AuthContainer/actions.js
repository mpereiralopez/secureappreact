import Service from './../../services';

const service = new Service();

export function isPosting(bool: boolean) {
  return {
    type: 'IS_POSTING',
    isLoading: bool,
  };
}
export function loginSuccess(userData: Object) {
  return {
    type: 'LOGIN_SUCCESS',
    userData,
    isLoading: false,
  };
}

export function registerSuccess(userData: Object) {
  return {
    type: 'REGISTER_SUCCESS',
    userData,
    isLoading: false,
  };
}

export function postFail(error: Object) {
  return {
    type: 'POST_FAIL',
    error,
    isLoading: false,
  };
}

export function doLogin(formData) {
  return async (dispatch) => {
    dispatch(isPosting(true));
    const { info, error } = await service.login(formData.password);
    if (error) {
      dispatch(postFail(error));
    } else {
      dispatch(loginSuccess(info));
    }
  };
}

export function doRegister(formData) {
  return async (dispatch) => {
    dispatch(isPosting(true));
    const userData = await service.registerUser(formData);
    const { info, error } = userData;
    if (error) {
      dispatch(postFail(error));
    } else {
      dispatch(registerSuccess(info));
    }
  };
}
