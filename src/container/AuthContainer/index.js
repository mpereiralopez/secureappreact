// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { Container, Content, Header, Body, Item, Title, Button, Input, Text, View, Icon, Form, Picker } from 'native-base';
import { Platform, Keyboard } from 'react-native';
import { email, required, alphaNumeric, minLength, isNumeric } from './validation';
import { doLogin, doRegister } from './actions';
import OverlaySpinner from './../../fragments/Overlay';

const loginForm = [
  {
    name: 'email',
    keyboardType: 'email-address',
    placeholder: 'Email',
    icon: 'person',
    validators: [email],
  },
  {
    name: 'password',
    secureTextEntry: true,
    placeholder: 'Password',
    icon: 'unlock',
    validators: [alphaNumeric, minLength(6), required],
  },
];

const registerForm = [...loginForm, {
  name: 'age',
  placeholder: 'Age',
  icon: 'pulse',
  keyboardType: 'numeric',
  validators: [isNumeric],
},
{
  name: 'gender',
  placeholder: 'Gender',
  icon: 'transgender',
  type: {
    id: 'select',
    values: ['no', 'female', 'male', 'other'],
  },
},
];

export interface Props {
  navigation: any;
  doLogin: Function;
  doRegister: Function;
  error: Object;
}
export interface State {
  fragment: String;
  errors: any;
  formData: any;
  toogleButtonText: String;
  submitButtonText: String;
}
class AuthContainer extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps) {
    const { userData, error } = nextProps;
    const hasErrors = (error && Object.keys(error).length !== 0);
    const hasData = (userData && Object.keys(userData).length !== 0);
    if (!hasErrors && hasData) nextProps.navigation.navigate('Home');
    return null;
  }
  constructor(props: Props) {
    super(props);
    this.state = {
      fragment: 'login',
      errors: {},
      formData: {},
      toogleButtonText: 'Switch to Register',
      submitButtonText: 'Login',
    };
    this.onValueChange = this.onValueChange.bind(this);
    this.login = this.login.bind(this);
  }

  onValueChange(val, ref) {
    const { validate, name } = ref.props;
    const msg = (validate) ? validate.map(rule =>
      rule(val)).find(res => res !== undefined) : undefined;
    const { errors, formData } = this.state;
    errors[name] = msg;
    if (val.trim().length > 0) formData[name] = val.trim();
    this.setState({ errors, formData });
  }

  login() {
    Keyboard.dismiss();
    const { formData, fragment } = this.state;
    const errors = {};
    let doPost = true;
    const template = (fragment === 'login') ? loginForm : registerForm;
    template.forEach((field) => {
      const val = formData[field.name];
      const err = (field.validators) ? field.validators.map(rule =>
        rule(val)).find(res => res !== undefined) : undefined;
      if (err) { errors[field.name] = err; doPost = false; }
    });
    this.setState({ errors });
    if (fragment === 'login' && doPost) this.props.doLogin(formData);
    if (fragment === 'register' && doPost) this.props.doRegister(formData);
  }

  switchForm() {
    let { fragment, submitButtonText } = this.state;
    fragment = (fragment === 'login') ? 'register' : 'login';
    submitButtonText = (fragment === 'login') ? 'Login' : 'Register';
    const toogleButtonText = (fragment === 'login') ? 'Switch to Register' : 'Switch to Login';
    const formData = {};
    const errors = {};
    if (fragment === 'register') {
      formData.gender = 'no';
    }
    this.setState({
      toogleButtonText,
      fragment,
      submitButtonText,
      formData,
      errors,
    });
  }

  renderInput({
    name,
    keyboardType,
    placeholder,
    icon,
    type,
    error,
    validators,
  }) {
    let input = (<Input
      ref={c => (this[`${name}Input`] = c)}
      name={name}
      placeholder={placeholder}
      secureTextEntry={name === 'password' || false}
      keyboardType={keyboardType}
      onChangeText={val => this.onValueChange(val, this[`${name}Input`])}
      validate={validators}
    />);
    if (type && type.id === 'select') {
      const vals = type.values.map(val => <Picker.Item key={val} label={val} value={val} />);
      input = (
        <Picker
          ref={c => (this[`${name}Input`] = c)}
          name={name}
          style={{ flex: 1 }}
          selectedValue={this.state.formData.gender}
          onValueChange={val => this.onValueChange(val, this[`${name}Input`])}
        >
          {vals}
        </Picker>
      );
    }

    return (
      <Item error={error && error.length > 0} key={name}>
        <Icon active name={icon} />
        {input}
      </Item>
    );
  }

  render() {
    console.log('AUTH CONTAINER');
    const { isLoading, error } = this.props;
    console.log(this.props);
    const { fragment, errors } = this.state;
    let form = loginForm;
    if (fragment === 'register') form = registerForm;
    return (
      <Container>
        {/* isLoading && <OverlaySpinner visible={isLoading} /> */}
        <Header style={{ height: 200 }}>
          <Body style={{ alignItems: 'center' }}>
            <Icon name="flash" style={{ fontSize: 104 }} />
            <Title>ReactNativeSeed.com</Title>
            <View padder>
              <Text style={{ color: Platform.OS === 'ios' ? '#000' : '#FFF' }}>
                Build Something Amazing
              </Text>
            </View>
          </Body>
        </Header>
        <Content>
          <Form>
            {form.map((ele) => {
              ele.error = errors[ele.name];
              return this.renderInput(ele);
            })}
          </Form>
          <View padder>
            {!isLoading && error && <Text>{error.message}</Text>}
          </View>
          <View padder>
            <Button block onPress={() => { this.switchForm(); }}>
              <Icon name="switch" />
              <Text>{this.state.toogleButtonText}</Text>
            </Button>
          </View>
          <View padder>
            <Button block onPress={() => { this.login(); }}>
              <Text>{this.state.submitButtonText}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    doLogin: formData => dispatch(doLogin(formData)),
    doRegister: formData => dispatch(doRegister(formData)),
  };
}

const mapStateToProps = state => ({
  isLoading: state.authReducer.isLoading,
  error: state.authReducer.error,
  userData: state.authReducer.userData,
});

export default connect(mapStateToProps, bindAction)(AuthContainer);

