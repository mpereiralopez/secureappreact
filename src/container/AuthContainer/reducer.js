const initialState = {
  isLoading: false,
  userData: {},
  error: {},
};

export default function (state: any = initialState, action: Function) {
  console.log(action);
  switch (action.type) {
    case 'IS_POSTING':
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case 'POST_FAIL':
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoading: false,
        userData: action.userData,
      };

    case 'REGISTER_SUCCESS':
      return {
        ...state,
        isLoading: false,
        userData: action.userData,
      };

    default:
      return state;
  }
}
