const required = value => (value ? undefined : 'Required');
const maxLength = max => value => (value && value.length > max ? `Must be ${max} characters or less` : undefined);
const minLength = min => value =>
  (value && value.length < min ? `Must be ${min} characters or more` : undefined);
const email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined);

const alphaNumeric = value => (
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined);

const isNumeric = value => ((!Number.isNaN(parseFloat(value)) && Number.isFinite(value)) ? 'Only numbers are allowed' : undefined);

module.exports = {
  required,
  maxLength,
  minLength,
  email,
  alphaNumeric,
  isNumeric,
};
