import { fetchEventTypeList } from './../HomeContainer/actions';

export default async function fetchEventTypeListSelect() {
  const payload = await fetchEventTypeList();
  return payload;
}

