// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import EventFrangment from '../../fragments/Event';
// import { fetchEventTypeListSelect } from './actions';

export interface Props {
  navigation: any,
  eventList: Array,
}
export interface State { }
class EventContainer extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps) {
    console.log('EventContainer getDerivedStateFromProps');
    console.log(nextProps);
    return null;
  }

  constructor(props: Props) {
    super(props);
    console.log('EventContainer CONSTRUCTOR');
    console.log(props);
    this.state = {};
  }

  render() {
    return <EventFrangment navigation={this.props.navigation} eventList={this.props.eventList} />;
  }
}

function bindAction(dispatch) {
  return {
    // fetchEventTypeList: () => dispatch(fetchEventTypeList()),
    // fetchEventsData: () => dispatch(fetchEventsData()),
  };
}

const mapStateToProps = state => ({
  eventList: state.homeReducer.list,
  isLoading: state.homeReducer.isLoading,
  tab: state.footerReducer.tab,
  geoData: state.homeReducer.geoData,
});
export default connect(mapStateToProps, bindAction)(EventContainer);
