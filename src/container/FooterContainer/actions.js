export function changeFooterTab(tab: String) {
  return {
    type: 'CHANGE_FOOTER_TAB',
    tab,
  };
}
export default changeFooterTab;
