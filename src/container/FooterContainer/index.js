// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import FooterTab from '../../fragments/FooterTab';
import { changeFooterTab } from './actions';

export interface Props {
  navigation: any;
  tab: String;
  changeFooterTab: Function;
}
export interface State { }
class FooterContainer extends React.Component<Props, State> {
  constructor(props) {
    super(props);
  }

  /* componentDidUpdate(nextProps) {
    console.log('FOOTER CONTAINER componenWillUpdate');
    console.log(nextProps);
  } */

  render() {
    return <FooterTab
      navigation={this.props.navigation}
      tab={this.props.tab}
      changeFooterTab={this.props.changeFooterTab} />;
  }
}

function bindAction(dispatch) {
  return {
    changeFooterTab: newTab => dispatch(changeFooterTab(newTab)),
  };
}

const mapStateToProps = state => ({
  tab: state.footerReducer.tab,
});
export default connect(mapStateToProps, bindAction)(FooterContainer);
