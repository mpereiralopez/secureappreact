const initialState = {
  tab: 'Home',
};

export default function (state: any = initialState, action: Function) {
  switch (action.type) {
    case 'CHANGE_FOOTER_TAB':
      state = {
        ...state,
        tab: action.tab,
      };
      break;
    default:
      break;
  }
  return state;
}
