import Service from './../../services';

const service = new Service();

export function listIsLoading(bool: boolean) {
  return {
    type: 'LIST_IS_LOADING',
    isLoading: bool,
  };
}
export function fetchListSuccess(list: Object) {
  return {
    type: 'FETCH_LIST_SUCCESS',
    list,
  };
}

export function fetchGeoDataSuccess(data: Object) {
  return {
    type: 'FETCH_GEODATA_SUCCESS',
    data,
  };
}

export function fetchListFail(error: Object) {
  return {
    type: 'FETCH_LIST_FAIL',
    error,
  };
}

export function fetchEventTypeList() {
  return async (dispatch) => {
    dispatch(listIsLoading(true));
    const { data } = await service.getEventTypeList();
    dispatch(fetchListSuccess(data));
  };
}


/* export function fetchEventsData() {
  console.log('fetchEventsData');
  return async (dispatch) => {
    // dispatch(listIsLoading(true));
    const userInfo = await AsyncStorage.getItem(`${APP_DB_KEY}userInfo`);
    console.log(userInfo);
    let { status, body } = await fetchGeoData(userInfo);
    console.log(body);
    if (status === 401 && body.message === 'jwt expired') {
      console.log('I NEED TO REFRESH TOKEN');
      const refreshedUserInfo = await refreshToken(userInfo);
      await AsyncStorage.setItem(`${APP_DB_KEY}userInfo`, JSON.stringify(refreshedUserInfo));
      const payload = await fetchGeoData(refreshedUserInfo);
      console.log(payload);
      body = payload.body;
    }
    console.log(body);
    dispatch(fetchGeoDataSuccess(body));
  };
} */
