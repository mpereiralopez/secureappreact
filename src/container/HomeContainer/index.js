// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import Home from '../../fragments/Home';
import { fetchEventTypeList, fetchEventsData } from './actions';

export interface Props {
  navigation: any,
  fetchEventTypeList: Function,
  fetchEventsData: Function,
  data: Array,
}
export interface State { }
class HomeContainer extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps) {
    console.log('HomeContainer getDerivedStateFromProps');
    console.log(nextProps);
    return null;
  }

  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    console.log('HomeContainer - componentDidMount');
    this.props.fetchEventTypeList();
    // this.props.fetchEventsData();
  }

  render() {
    return <Home navigation={this.props.navigation} list={this.props.data} tab={this.props.tab} />;
  }
}

function bindAction(dispatch) {
  return {
    fetchEventTypeList: () => dispatch(fetchEventTypeList()),
    fetchEventsData: () => dispatch(fetchEventsData()),
  };
}

const mapStateToProps = state => ({
  data: state.homeReducer.list,
  isLoading: state.homeReducer.isLoading,
  tab: state.footerReducer.tab,
  geoData: state.homeReducer.geoData,
});
export default connect(mapStateToProps, bindAction)(HomeContainer);
