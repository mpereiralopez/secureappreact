const initialState = {
  list: [],
  isLoading: true,
  userData: {},
  error: {},
  geoData: [],
};

export default function (state: any = initialState, action: Function) {
  console.log(action);
  switch (action.type) {
    case 'FETCH_LIST_SUCCESS':
      return {
        ...state,
        list: action.list,
      };
    case 'LIST_IS_LOADING':
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case 'FETCH_LIST_FAIL':
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case 'FETCH_GEODATA_SUCCESS':
      return {
        ...state,
        isLoading: false,
        error: action.error,
        geoData: action.geoData,
      };
    case 'RETREIVE_USER_DATA':
      return {
        ...state,
        isLoading: false,
        userData: action.userData,
        error: action.error,
      };
    default:
      return state;
  }
}
