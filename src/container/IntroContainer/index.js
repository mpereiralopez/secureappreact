// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import Realm from 'realm';
import Splash from '../../fragments/Splash';
import { UserActions } from './../../actions';

export interface Props {
  navigation: any,
  fetchEventTypeList: Function,
  fetchEventsData: Function,
  data: Array,
  userData: any,
}
export interface State { }
class IntroContainer extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('getDerivedStateFromProps');
    console.log(nextProps);
    console.log(prevState);
    // prevState.userData = nextProps.userData || null;
    return null;
  }

  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getUserInfo();
  }

  render() {
    return (<Splash
      isLoading={this.props.isLoading}
      navigation={this.props.navigation}
      userData={this.props.userData || null}
    />);
  }
}

function bindAction(dispatch) {
  const ua = new UserActions();
  return {
    getUserInfo: () => dispatch(ua.getUserFromDB()),
  };
}

const mapStateToProps = state => ({
  userData: state.homeReducer.userData,
  isLoading: state.homeReducer.isLoading,
});
export default connect(mapStateToProps, bindAction)(IntroContainer);
