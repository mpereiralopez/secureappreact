import React, { Component } from 'react';

import {
  View,
  Text,
  Modal,
  DatePickerAndroid,
  TimePickerAndroid,
  DatePickerIOS,
  Platform,
  Animated,
  Keyboard,
} from 'react-native';
import { Button, Icon } from 'native-base';
import Moment from 'moment';

import Style from './style';

const FORMATS = {
  date: 'YYYY-MM-DD',
  datetime: 'YYYY-MM-DD HH:mm',
  time: 'HH:mm',
};

const SUPPORTED_ORIENTATIONS = ['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right'];
// mode: PropTypes.oneOf(['date', 'datetime', 'time'])
// androidMode: PropTypes.oneOf(['calendar', 'spinner', 'default']),
export interface Props {
  mode: String;
  androidMode: String;
  date: any;
  format: String;
  minDate: any;
  maxDate: any;
  height: Number;
  duration: Number;
  confirmBtnText: String;
  cancelBtnText: String;
  iconSource: any;
  iconComponent: any;
  showIcon: Boolean;
  disabled: Boolean;
  onDateChange: Function;
  onOpenModal: Function;
  onCloseModal: Function;
  onPressMask: Function;
  placeholder: String;
  modalOnResponderTerminationRequest: Function;
  is24Hour: Boolean;
}

export interface State {
  date: any;
  modalVisible: Boolean;
  allowPointerEvents: Boolean;
  animatedHeight: any;
}

class DatePicker extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      date: this.getDate(),
      modalVisible: false,
      animatedHeight: new Animated.Value(0),
      allowPointerEvents: true,
    };

    this.getDate = this.getDate.bind(this);
    this.getDateStr = this.getDateStr.bind(this);
    this.datePicked = this.datePicked.bind(this);
    this.onPressDate = this.onPressDate.bind(this);
    this.onPressCancel = this.onPressCancel.bind(this);
    this.onPressConfirm = this.onPressConfirm.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onPressMask = this.onPressMask.bind(this);
    this.onDatePicked = this.onDatePicked.bind(this);
    this.onTimePicked = this.onTimePicked.bind(this);
    this.onDatetimePicked = this.onDatetimePicked.bind(this);
    this.onDatetimeTimePicked = this.onDatetimeTimePicked.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
  }

  componentWillMount() {
    if (!console.ignoredYellowBox) {
      console.ignoredYellowBox = [];
    }
    console.ignoredYellowBox.push('Warning: Failed propType');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.date !== this.props.date) {
      this.setState({ date: this.getDate(nextProps.date) });
    }
  }

  /*
  onStartShouldSetResponder(e) {
    return true;
  }

  onMoveShouldSetResponder(e) {
    return true;
  }
  */

  onPressMask() {
    if (typeof this.props.onPressMask === 'function') {
      this.props.onPressMask();
    } else {
      this.onPressCancel();
    }
  }

  onPressCancel() {
    this.setModalVisible(false);

    if (typeof this.props.onCloseModal === 'function') {
      this.props.onCloseModal();
    }
  }

  onPressConfirm() {
    this.datePicked();
    this.setModalVisible(false);

    if (typeof this.props.onCloseModal === 'function') {
      this.props.onCloseModal();
    }
  }

  onDatetimePicked({
    action,
    year,
    month,
    day,
  }) {
    const {
      mode,
      androidMode,
      format = FORMATS[mode],
      is24Hour = !format.match(/h|a/),
    } = this.props;
    if (action !== DatePickerAndroid.dismissedAction) {
      const timeMoment = Moment(this.state.date);
      TimePickerAndroid.open({
        hour: timeMoment.hour(),
        minute: timeMoment.minutes(),
        is24Hour,
        mode: androidMode,
      }).then(this.onDatetimeTimePicked.bind(this, year, month, day));
    } else {
      this.onPressCancel();
    }
  }


  onDateChange(date) {
    this.setState({
      allowPointerEvents: false,
      date,
    });
    const timeoutId = setTimeout(() => {
      this.setState({ allowPointerEvents: true });
      clearTimeout(timeoutId);
    }, 200);
  }

  onTimePicked({ action, hour, minute }) {
    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        date: Moment().hour(hour).minute(minute).toDate(),
      });
      this.datePicked();
    } else {
      this.onPressCancel();
    }
  }

  onDatePicked({
    action,
    year,
    month,
    day,
  }) {
    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        date: new Date(year, month, day),
      });
      this.datePicked();
    } else {
      this.onPressCancel();
    }
  }

  onDatetimeTimePicked(year, month, day, { action, hour, minute }) {
    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        date: new Date(year, month, day, hour, minute),
      });
      this.datePicked();
    } else {
      this.onPressCancel();
    }
  }

  onPressDate() {
    console.log('onPressDate');
    if (this.props.disabled) {
      return true;
    }

    Keyboard.dismiss();

    // reset state
    this.setState({ date: this.getDate() });

    if (Platform.OS === 'ios') {
      this.setModalVisible(true);
    } else {
      const {
        mode,
        androidMode,
        format = FORMATS[mode],
        minDate, maxDate,
        is24Hour = !format.match(/h|a/),
      } = this.props;

      if (mode === 'date') {
        DatePickerAndroid.open({
          date: this.state.date,
          minDate: minDate && this.getDate(minDate),
          maxDate: maxDate && this.getDate(maxDate),
          mode: androidMode,
        }).then(this.onDatePicked);
      } else if (mode === 'time') {
        const timeMoment = Moment(this.state.date);
        TimePickerAndroid.open({
          hour: timeMoment.hour(),
          minute: timeMoment.minutes(),
          is24Hour,
        }).then(this.onTimePicked);
      } else if (mode === 'datetime') {
        DatePickerAndroid.open({
          date: this.state.date,
          minDate: minDate && this.getDate(minDate),
          maxDate: maxDate && this.getDate(maxDate),
          mode: androidMode,
        }).then(this.onDatetimePicked);
      }
    }

    if (typeof this.props.onOpenModal === 'function') {
      this.props.onOpenModal();
    }
    return true;
  }

  setModalVisible(visible) {
    const { height, duration } = this.props;

    // slide animation
    if (visible) {
      this.setState({ modalVisible: visible });
      return Animated.timing(
        this.state.animatedHeight,
        {
          toValue: height,
          duration,
        },
      ).start();
    }
    return Animated.timing(
      this.state.animatedHeight,
      {
        toValue: 0,
        duration,
      },
    ).start(() => {
      this.setState({ modalVisible: visible });
    });
  }

  getDate(date = this.props.date) {
    const {
      mode,
      minDate,
      maxDate,
      format = FORMATS[mode],
    } = this.props;
    if (!date) {
      const now = new Date();
      if (minDate) {
        const localMinDate = this.getDate(minDate);
        if (now < localMinDate) {
          return localMinDate;
        }
      }
      if (maxDate) {
        const localMaxDate = this.getDate(maxDate);
        if (now > localMaxDate) {
          return localMaxDate;
        }
      }
      return now;
    }
    if (date instanceof Date) {
      return date;
    }
    return Moment(date, format).toDate();
  }


  getDateStr(date = this.props.date) {
    const { mode, format = FORMATS[mode] } = this.props;

    if (date instanceof Date) {
      return Moment(date).format(format);
    }
    return Moment(this.getDate(date)).format(format);
  }


  getTitleElement() {
    const { date, placeholder } = this.props;
    console.log(date);
    console.log(placeholder);
    if (!date && placeholder) {
      return (
        <Text style={[Style.placeholderText]}>{placeholder}</Text>
      );
    }
    return (<Text style={[Style.dateText]}>{this.getDateStr()}</Text>);
  }

  datePicked() {
    if (typeof this.props.onDateChange === 'function') {
      this.props.onDateChange(this.getDateStr(this.state.date), this.state.date);
    }
  }

  render() {
    const {
      mode,
      disabled,
      minDate,
      maxDate,
      minuteInterval,
      timeZoneOffsetInMinutes,
      cancelBtnText,
      confirmBtnText,
      TouchableComponent,
      testID,
      cancelBtnTestID,
      confirmBtnTestID,
    } = this.props;

    return (
      <View style={[Style.dateTouchBody]}>
        <Button
          testID={testID}
          rounded
          bordered
          block
          warning
          onPress={this.onPressDate}
        >
          <Icon name="calendar" />
          {this.getTitleElement()}
        </Button>

        {Platform.OS === 'ios' &&
          <Modal
            transparent
            animationType="none"
            visible={this.state.modalVisible}
            supportedOrientations={SUPPORTED_ORIENTATIONS}
            onRequestClose={() => { this.setModalVisible(false); }}
          >
            <View
              style={{ flex: 1 }}
            >
              <TouchableComponent
                style={Style.datePickerMask}
                activeOpacity={1}
                underlayColor="#00000077"
                onPress={this.onPressMask}
              >
                <TouchableComponent
                  underlayColor="#fff"
                  style={{ flex: 1 }}
                >
                  <Animated.View
                    style={[
                      Style.datePickerCon,
                      { height: this.state.animatedHeight },
                    ]}
                  >
                    <View pointerEvents={this.state.allowPointerEvents ? 'auto' : 'none'}>
                      <DatePickerIOS
                        date={this.state.date}
                        mode={mode}
                        minimumDate={minDate && this.getDate(minDate)}
                        maximumDate={maxDate && this.getDate(maxDate)}
                        onDateChange={this.onDateChange}
                        minuteInterval={minuteInterval}
                        timeZoneOffsetInMinutes={timeZoneOffsetInMinutes}
                        style={[Style.datePicker]}
                      />
                    </View>
                    <TouchableComponent
                      underlayColor="transparent"
                      onPress={this.onPressCancel}
                      style={[Style.btnText, Style.btnCancel]}
                      testID={cancelBtnTestID}
                    >
                      <Text
                        style={[
                          Style.btnTextText,
                          Style.btnTextCancel,
                        ]}
                      >
                        {cancelBtnText}
                      </Text>
                    </TouchableComponent>
                    <TouchableComponent
                      underlayColor="transparent"
                      onPress={this.onPressConfirm}
                      style={[Style.btnText, Style.btnConfirm]}
                      testID={confirmBtnTestID}
                    >
                      <Text
                        style={[Style.btnTextText]}
                      >{confirmBtnText}
                      </Text>
                    </TouchableComponent>
                  </Animated.View>
                </TouchableComponent>
              </TouchableComponent>
            </View>
          </Modal>}
      </View>
    );
  }
}

export default DatePicker;
