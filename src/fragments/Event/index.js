import * as React from 'react';
import { View } from 'react-native';
import moment from 'moment';
import { Container, Label, List, ListItem, Text, Content, Thumbnail, Button, Icon, Body, Switch, Item, Input } from 'native-base';
import StepIndicator from './../Steps';
import DatePicker from './../DatePicker';

import styles from './styles';

export interface Props {
  navigation: any;
  eventList: Array;
  deviceId: String;
}
export interface State {
  currentPosition: Number;
  eventSelected: any;
  nextEnable: false;
  chosenDate: any;
}

class EventFrangment extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      currentPosition: 0,
      eventSelected: null,
      nextEnable: false,
      chosenDate: null,
    };
    this.renderStep = this.renderStep.bind(this);
    this.onEventSelected = this.onEventSelected.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  onPageChange(position) {
    this.setState({ currentPosition: position });
  }

  onEventSelected(val) {
    const prev = this.state.eventSelected;
    if (prev === val) {
      this.setState({ eventSelected: null, nextEnable: false });
    } else {
      this.setState({ eventSelected: val, nextEnable: true });
    }
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  renderStep() {
    const { currentPosition, eventSelected } = this.state;
    if (currentPosition === 0) {
      const items = this.props.eventList.map(eventType => (
        <ListItem key={eventType.title}>
          <Thumbnail square size={80} source={{ uri: eventType.iconData.data }} />
          <Body>
            <Text>{eventType.title}</Text>
            <Text note>{eventType.description}</Text>
          </Body>
          <Switch
            value={eventSelected !== null && eventSelected === eventType._id}
            onValueChange={() => this.onEventSelected(eventType._id)}
          />
        </ListItem>
      ));
      return (
        <List>
          {items}
        </List>
      );
    }
    if (currentPosition === 1) {
      return (
        <View>
          <Label style={{ textAlign: 'center' }}>Inform when the event happens</Label>
          <DatePicker
            date={this.state.chosenDate}
            mode="datetime"
            format="YYYY-MM-DD HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            placeholder="When it happens?"
            maxDate={new Date()}
            minuteInterval={10}
            onDateChange={(datetime) => { this.setState({ chosenDate: datetime }); }}
          />
        </View>
      );
    }
    if (currentPosition === 2) {
      return (
        <Item rounded>
          <Input
            placeholder="Extra info"
            maxLength={150}
            multiline
            numberOfLines={5}
          />
        </Item>
      );
    }
    return true;
  }

  render() {
    const labels = ['What happend?', 'Where & When it happens?', 'Post'];
    return (
      <Container style={{ flex: 1 }}>
        <View style={styles.closeButton}>
          <Button
            block
            rounded
            onPress={() => this.props.navigation.navigate('Home')}
          >
            <Icon name="close" />
          </Button>
        </View>
        <Content style={{ flex: 2 }} padder>
          <StepIndicator
            currentPosition={this.state.currentPosition}
            labels={labels}
            stepCount={labels.length}
          />
          {this.renderStep()}
        </Content>
        <View style={styles.navButtons}>
          <Button
            block
            disabled={this.state.currentPosition === 0}
            rounded
            onPress={() => this.onPageChange(this.state.currentPosition - 1)}
          >
            <Icon name="arrow-back" />
            <Text>Back</Text>
          </Button>
          <Button
            block
            disabled={this.state.currentPosition === labels.length - 1 || !this.state.nextEnable}
            rounded
            onPress={() => this.onPageChange(this.state.currentPosition + 1)}
          >
            <Text>Next</Text>
            <Icon name="arrow-forward" />
          </Button>
        </View>
      </Container>
    );
  }
}

export default EventFrangment;
