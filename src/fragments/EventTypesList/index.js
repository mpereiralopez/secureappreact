import * as React from 'react';
import { WebView, Image } from 'react-native';
import { Content, Right, Card, CardItem, H2, Text, Button, Icon, Left, Body, Thumbnail } from 'native-base';

export interface Props {
  list: any;
}
export interface State {
  list: any;
}
class EventTypesList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { list: props.list };
  }
  render() {
    console.log(this.state);
    return (
      <Content>
        {
          this.state.list.map(eventType => (
            <Card key={eventType.title}>
              <CardItem>
                <Left>
                  <Thumbnail source={{ uri: eventType.iconData.data }} />
                  <Body>
                    <Text>{eventType.title}</Text>
                    {/* <Text note>GeekyAnts</Text> */}
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <Text>{eventType.description}</Text>
              </CardItem>
              {/* <CardItem>
                <Left>
                  <Button transparent>
                    <Icon active name="thumbs-up" />
                    <Text>12 Likes</Text>
                  </Button>
                </Left>
                <Body>
                  <Button transparent>
                    <Icon active name="chatbubbles" />
                    <Text>4 Comments</Text>
                  </Button>
                </Body>
                <Right>
                  <Text>11h ago</Text>
                </Right>
              </CardItem> */ }
            </Card>
          ))
        }
      </Content>
    );
  }
}

export default EventTypesList;
