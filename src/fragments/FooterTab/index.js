import * as React from 'react';
import { Footer, FooterTab, Text, Button, Icon } from 'native-base';

// import styles from './styles';
export interface Props {
  navigation: any;
  tab: String;
  changeFooterTab: Function;
}
export interface State { screen: String }
class FooterTabComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      screen: this.props.tab,
    };
  }

  componentDidUpdate(nextProps) {
    this.state.screen = nextProps.tab;
  }

  render() {
    return (
      <Footer>
        <FooterTab>
          <Button onPress={() => this.props.changeFooterTab('Home')} active={this.state.screen === 'Home'} vertical>
            <Icon name="map" />
            <Text>Maps</Text>
          </Button>
          <Button onPress={() => this.props.changeFooterTab('EventTypesList')} active={this.state.screen === 'EventTypesList'} vertical>
            <Icon name="list" />
            <Text>List</Text>
          </Button>
          <Button onPress={() => this.props.changeFooterTab('EventTypeDetail')} active={this.state.screen === 'EventTypeDetail'} vertical>
            <Icon name="ios-paper" />
            <Text>Detail</Text>
          </Button>
          <Button onPress={() => this.props.changeFooterTab('EmergencyContact')} active={this.state.screen === 'EmergencyContact'} vertical>
            <Icon name="ios-information-circle" />
            <Text>Info</Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

export default FooterTabComponent;
