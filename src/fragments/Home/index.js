import * as React from 'react';
import {
  Container,
  Content,
  Text,
  Icon,
  Button,
} from 'native-base';
import Notification from 'react-native-in-app-notification';

import FooterContainer from './../../container/FooterContainer';
import EventTypesList from './../EventTypesList';
import styles from './styles';

export interface Props {
  navigation: any;
  list: any;
  error: Object;
}
export interface State {
  notification: any;
  fabActive: boolean;
}

class Home extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('Home fragment getDerivedStateFromProps');
    console.log(nextProps);
    console.log(prevState);
    if (prevState.notification.show) {
      prevState.notification.show({
        title: 'You pressed it!',
        message: 'The notification has been triggered',
        onPress: () => console.log('WEHEHEHE'),
      });
    }
    return prevState;
  }
  constructor(props: Props) {
    super(props);
    this.state = {
      notification: {},
    };
    this.renderEventTypesList = this.renderEventTypesList.bind(this);
  }

  componentDidMount() {
    console.log('componentDidMount');
    console.log(this.notification);
    this.state.notification = this.notification;
  }

  renderEventTypesList() {
    return (<EventTypesList list={this.props.list} />);
  }

  render() {
    return (
      <Container style={styles.container}>
        <Notification backgroundColour="red" ref={ref => this.notification = ref} />
        <Content>

          {
            this.props.tab === 'EventTypesList' && this.renderEventTypesList()
          }
        </Content>

        <Button
          block
          rounded
          success
          style={{ bottom: 10 }}
          onPress={() => this.props.navigation.navigate('Events')}
        >
          <Icon name="add" />
          <Text>Add event</Text>
        </Button>
        <FooterContainer
          tab={this.props.tab}
          navigation={this.props.navigation}
        />
      </Container>
    );
  }
}

export default Home;
