import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  ActivityIndicator,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  textContent: {
    top: 80,
    height: 50,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

const ANIMATION = ['none', 'slide', 'fade'];
const SIZES = ['small', 'normal', 'large'];

export interface Props {
  visible: Boolean;
  cancelable: Boolean,
  textContent: String;
  animation: String;
  color: String;
  size: String,
  overlayColor: String;
}

class OverlaySpinner extends React.Component<Props> {
  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('getDerivedStateFromProps');
    console.log(nextProps);
    console.log(prevState);
    const { visible, textContent } = nextProps;
    const newState = { ...prevState, visible, textContent };
    // this.setState({ visible, textContent });
    // prevState.userData = nextProps.userData || null;
    return newState;
  }
  static defaultProps = {
    visible: false,
    cancelable: false,
    textContent: '',
    animation: 'fade',
    color: 'white',
    size: 'large', // 'normal',
    overlayColor: 'rgba(0, 0, 0, 0.25)',
  };

  constructor(props) {
    super(props);
    this.state = { visible: this.props.visible, textContent: this.props.textContent };
  }

  /* componentWillReceiveProps(nextProps) {
    const { visible, textContent } = nextProps;
    this.setState({ visible, textContent });
  } */

  close() {
    this.setState({ visible: false });
  }

  handleOnRequestClose() {
    if (this.props.cancelable) {
      this.close();
    }
  }

  renderDefaultContent() {
    return (
      <View style={styles.background}>
        <ActivityIndicator
          color={this.props.color}
          size={this.props.size}
          style={{ flex: 1 }}
        />
        <View style={styles.textContainer}>
          <Text style={[styles.textContent, this.props.textStyle]}>{this.state.textContent}</Text>
        </View>
      </View>);
  }

  renderSpinner() {
    const { visible } = this.state;
    if (!visible) {
      return null;
    }
    const spinner = (
      <View
        style={[
          styles.container,
          { backgroundColor: this.props.overlayColor },
        ]}
        key={`spinner_${Date.now()}`}
      >
        {this.props.children ? this.props.children : this.renderDefaultContent()}
      </View>
    );

    return (
      <Modal
        animationType={this.props.animation}
        onRequestClose={() => this.handleOnRequestClose()}
        supportedOrientations={['landscape', 'portrait']}
        transparent
        visible={visible}
      >
        {spinner}
      </Modal>
    );
  }
  render() {
    return this.renderSpinner();
  }
}

export default OverlaySpinner;

