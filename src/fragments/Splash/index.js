import * as React from 'react';
import { Container } from 'native-base';

import SplashComponent from './screens/splash';
import Slider from './screens/introSlide';

export interface Props {
  navigation: any;
}
export interface State {
  actual: String;
}

class Splash extends React.Component<Props, State> {
  static getDerivedStateFromProps(nextProps, prevState) {
    const nextState = { ...prevState };
    const userData = nextProps.userData || null;
    if (nextProps.isLoading === false && userData === null) {
      nextState.actual = 'slider';
    } else if (nextProps.isLoading === false && userData !== null) {
      nextProps.navigation.navigate('Home');
    }
    return nextState;
  }
  constructor(props: Props) {
    super(props);
    this.state = {
      actual: 'splash',
    };
  }

  componentDidMount() {
    // window.setTimeout(() => { this.setState({ actual: 'slider' }); }, 2000);
  }

  render() {
    let render = (<SplashComponent />);
    if (this.state.actual === 'slider') {
      render = (<Slider onDone={() => this.props.navigation.navigate('Auth')} />);
    }
    return (
      <Container style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      >
        {render}
      </Container>
    );
  }
}

export default Splash;
