import React from 'react';
// https://github.com/Jacse/react-native-app-intro-slider
import {
  StyleSheet,
  FlatList,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Platform,
  StatusBar,
} from 'react-native';
import DefaultSlide from './defaultSlide';

const { width, height } = Dimensions.get('window');

const isIphoneX = (
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812)
);

const styles = StyleSheet.create({
  image: {
    width: 320,
    height: 320,
  },
  flexOne: {
    flex: 1,
  },
  paginationContainer: {
    position: 'absolute',
    bottom: 16 + (isIphoneX ? 34 : 0),
    left: 0,
    right: 0,
  },
  paginationDots: {
    height: 16,
    margin: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  leftButtonContainer: {
    position: 'absolute',
    left: 0,
  },
  rightButtonContainer: {
    position: 'absolute',
    right: 0,
  },
  bottomButtonContainer: {
    height: 44,
    marginHorizontal: 16,
  },
  bottomButton: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, .3)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 18,
    padding: 16,
  },
});

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    image: require('./../bag-snatching.png'),
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Other cool stuff',
    image: require('./../bag-snatching.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 'somethun1',
    title: 'Rocket guy',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    image: require('./../bag-snatching.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
];

export interface Props {
  onSlideChange: Function;
  slides: Array;
}

export default class AppIntroSlider extends React.Component<Props> {
  static defaultProps = {
    activeDotColor: 'rgba(255, 255, 255, .9)',
    dotColor: 'rgba(0, 0, 0, .2)',
    skipLabel: 'Skip',
    doneLabel: 'Done',
    nextLabel: 'Next',
    prevLabel: 'Back',
  }
  constructor(props: Props) {
    super(props);
    this.slides = slides;
    this.state = {
      width,
      height,
      activeIndex: 0,
    };
  }


  onMomentumScrollEnd = (e) => {
    const offset = e.nativeEvent.contentOffset.x;
    // Touching very very quickly and continuous brings about
    // a variation close to - but not quite - the width.
    // That's why we round the number.
    // Also, Android phones and their weird numbers
    const newIndex = Math.round(offset / this.state.width);
    if (newIndex === this.state.activeIndex) {
      // No page change, don't do anything
      return;
    }
    const lastIndex = this.state.activeIndex;
    this.setState({ activeIndex: newIndex });
    this.props.onSlideChange && this.props.onSlideChange(newIndex, lastIndex);
  }

  onLayout = () => {
    if (width !== this.state.width || height !== this.state.height) {
      // Set new width to update rendering of pages
      this.setState({ width, height });
      // Set new scroll position
      const func = () => { this.flatList.scrollToOffset({ offset: this.state.activeIndex * width, animated: false }); };
      Platform.OS === 'android' ? setTimeout(func, 0) : func();
    }
  }

  onNextPress = () => {
    this.goToSlide(this.state.activeIndex + 1);
    this.props.onSlideChange && this.props.onSlideChange(this.state.activeIndex + 1, this.state.activeIndex);
  }

  onPrevPress = () => {
    this.goToSlide(this.state.activeIndex - 1);
    this.props.onSlideChange && this.props.onSlideChange(this.state.activeIndex - 1, this.state.activeIndex);
  }

  goToSlide = (pageNum) => {
    this.setState({ activeIndex: pageNum });
    this.flatList.scrollToOffset({ offset: pageNum * this.state.width });
  }

  renderItem = (item) => {
    const bottomSpacer = (this.props.bottomButton ? (this.props.showSkipButton ? 44 : 0) + 44 : 0) + (isIphoneX ? 34 : 0) + 64;
    const topSpacer = (isIphoneX ? 44 : 0) + (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight);
    const props = {
      ...item.item,
      bottomSpacer,
      topSpacer,
      width,
      height,
    };
    console.log('renderItem');
    console.log(props);
    return this.props.renderItem ? this.props.renderItem(props) : <DefaultSlide {...props} />;
  }

  renderButton = (name, onPress) => {
    const show = (name === 'Skip' || name === 'Prev') ? this.props[`show${name}Button`] : !this.props[`hide${name}Button`];
    const content = this.props[`render${name}Button`] ? this.props[`render${name}Button`]() : this.renderDefaultButton(name);
    return show && this.renderOuterButton(content, name, onPress);
  }

  renderDefaultButton = (name) => {
    let content = <Text style={styles.buttonText}>{this.props[`${name.toLowerCase()}Label`]}</Text>;
    if (this.props.bottomButton) {
      content = <View style={[styles.bottomButton, (name === 'Skip' || name === 'Prev') && { backgroundColor: 'transparent' }]}>{content}</View>
    }
    return content;
  }

  renderOuterButton = (content, name, onPress) => {
    const style = (name === 'Skip' || name === 'Prev') ? styles.leftButtonContainer : styles.rightButtonContainer;
    return (
      <View style={this.props.bottomButton ? styles.bottomButtonContainer : style}>
        <TouchableOpacity onPress={onPress} style={this.props.bottomButton && styles.flexOne}>
          {content}
        </TouchableOpacity>
      </View>
    );
  }

  renderNextButton = () => this.renderButton('Next', this.onNextPress)

  renderPrevButton = () => this.renderButton('Prev', this.onPrevPress)

  renderDoneButton = () => this.renderButton('Done', this.props.onDone && this.props.onDone)

  renderSkipButton = () => this.renderButton('Skip', this.props.onSkip && this.props.onSkip)

  renderPagination = () => {
    const isLastSlide = this.state.activeIndex === (this.slides.length - 1);
    const isFirstSlide = this.state.activeIndex === 0;

    const skipBtn = (!isFirstSlide && this.renderPrevButton()) || (!isLastSlide && this.renderSkipButton());
    const btn = isLastSlide ? this.renderDoneButton() : this.renderNextButton();

    return (
      <View style={styles.paginationContainer}>
        <View style={styles.paginationDots}>
          {!this.props.bottomButton && skipBtn}
          {this.slides.length > 1 && this.slides.map((_, i) => (
            <View
              key={i}
              style={[
                { backgroundColor: i === this.state.activeIndex ? this.props.activeDotColor : this.props.dotColor },
                styles.dot,
              ]}
            />
          ))}
          {!this.props.bottomButton && btn}
        </View>
        {this.props.bottomButton && btn}
        {this.props.bottomButton && skipBtn}
      </View>
    );
  }

  render() {
    console.log(this.props);
    return (
      <View style={styles.flexOne}>
        <FlatList
          ref={component => this.flatList = component}
          data={this.slides}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          bounces={false}
          style={styles.flexOne}
          renderItem={this.renderItem}
          onMomentumScrollEnd={this.onMomentumScrollEnd}
          extraData={this.state.width}
          onLayout={this.onLayout}
        />
        {this.renderPagination()}
      </View>
    );
  }
}
