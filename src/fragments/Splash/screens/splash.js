import * as React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';

const SplashComponent = () => (
  <View style={{ backgroundColor: 'powderblue' }}>
    <ActivityIndicator size="large" color="#0000ff" />
    <Text>This is a splash Screen</Text>
  </View>);

export default SplashComponent;
