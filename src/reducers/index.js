import { combineReducers } from 'redux';

import homeReducer from '../container/HomeContainer/reducer';
import footerReducer from '../container/FooterContainer/reducer';
import authReducer from '../container/AuthContainer/reducer';

export default combineReducers({
  homeReducer,
  footerReducer,
  authReducer,
});
