import DeviceInfo from 'react-native-device-info';

export default class ApiServices {
  constructor() {
    this.API_BASE = 'http://10.0.2.2:';
    this.API_PORT = '3002';
    this.API_VERSION = '/v1';
    this.EVENTS_TYPE = '/event-types';
    this.AUTH = '/auth';
    this.LOGIN = '/login';
    this.REGISTER = '/register';
    this.EVENTS = '/events';
    this.REFRESH_TOKEN = '/refresh-token';
  }

  static normalizeUser(status, response) {
    const payload = {};
    const { user, token } = response;
    if (status !== 201 && status !== 200) {
      payload.error = response;
    } else {
      payload.info = {
        accessToken: token.accessToken,
        refreshToken: token.refreshToken,
        id: user.id,
        createdAt: user.createdAt,
        email: user.email,
        name: user.name,
        deviceId: user.deviceId,
        password: user.password,
      };
    }
    return payload;
  }

  async login(userInfo) {
    const url = `${this.API_BASE}${this.API_PORT}${this.API_VERSION}${this.AUTH}${this.LOGIN}`;
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(userInfo),
    };
    let payload = {};
    try {
      const response = await fetch(url, config);
      const json = await response.json();
      json.user.password = userInfo.password;
      payload = ApiServices.normalizeUser(response.status, json);
    } catch (e) {
      payload.error = e;
    }
    return payload;
  }

  async registerUser(userInfo) {
    const url = `${this.API_BASE}${this.API_PORT}${this.API_VERSION}${this.AUTH}${this.REGISTER}`;
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(userInfo),
    };
    let payload = {};
    try {
      const response = await fetch(url, config);
      const json = await response.json();
      json.user.password = userInfo.password;
      payload = ApiServices.normalizeUser(response.status, json);
    } catch (e) {
      payload.error = e;
    }
    return payload;
  }

  async fetchEventTypes(userInfo) {
    console.log('fetchEventTypes');
    console.log(userInfo);
    let localeInfo = DeviceInfo.getDeviceLocale();
    if (localeInfo.indexOf('_') !== -1) {
      localeInfo = localeInfo.split('_')[0];
    }
    const url = `${this.API_BASE}${this.API_PORT}${this.API_VERSION}${this.EVENTS_TYPE}`;
    const config = {
      method: 'GET',
      headers: {
        'Accept-Language': localeInfo,
        Authorization: `Bearer ${userInfo.accessToken}`,
        'x-user-id': userInfo.id,
      },
    };
    const response = await fetch(url, config);
    const json = await response.json();
    return { status: response.status, body: json };
  }

  async refreshToken(userInfo) {
    console.log('======= refreshToken ========');
    console.log(userInfo);
    const refreshURL = `${this.API_BASE}${this.API_PORT}${this.API_VERSION}${this.AUTH}${this.REFRESH_TOKEN}`;
    const refreshConfig = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        refreshToken: userInfo.refreshToken,
        email: userInfo.email,
        deviceId: userInfo.deviceId,
      }),
    };
    const response = await fetch(refreshURL, refreshConfig);
    const json = await response.json();
    console.log(json);
    const refreshUser = { ...userInfo };
    refreshUser.accessToken = json.accessToken;
    refreshUser.refreshToken = json.refreshToken;
    console.log('API refreshToken');
    console.log(refreshUser);
    return { userInfo: refreshUser };
  }


  async fetchGeoData(userInfo) {
    const url = `${this.API_BASE}${this.API_PORT}${this.API_VERSION}${this.EVENTS}`;
    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${userInfo.accessToken}`,
        'x-user-id': userInfo.id,
      },
    };
    const response = await fetch(url, config);
    const json = await response.json();
    return { status: response.status, body: json };
  }
}
