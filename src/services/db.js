import Realm from 'realm';

const UserSchema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: 'string',
    deviceId: 'string',
    email: 'string',
    password: 'string',
    name: 'string',
    accessToken: 'string',
    refreshToken: 'string',
  },
};

export default class DBService {
  constructor() {
    this.DB = new Realm({
      schema: [UserSchema],
      schemaVersion: 2,
      migration: (oldRealm, newRealm) => {
        if (oldRealm.schemaVersion === 1) {
          // modelRename(oldRealm, newRealm);
          // oldRealm.deleteAll();
          newRealm.deleteAll();
        }
      },
    });
  }

  insertUser(info) {
    try {
      this.DB.write(() => this.DB.create('User', info));
      return { info };
    } catch (error) {
      console.log('-----------insertUser- CATCH-------------');
      console.log(error);
      return { error };
    }
  }

  updateUser(info) {
    try {
      this.DB.write(() => this.DB.create('User', info, true));
      return { info };
    } catch (error) {
      console.log('-----------updateUser- CATCH-------------');
      console.log(error);
      return { error };
    }
  }

  retrieveUser() {
    try {
      const userInfoRealm = this.DB.objects('User')[0];
      if (userInfoRealm.length === 0 || !userInfoRealm) {
        const error = { message: 'No user at device' };
        return { error };
      }
      const plain = { ...UserSchema.properties };
      const userInfo = (({ ...plain }) => (plain))(userInfoRealm);
      return { userInfo };
    } catch (error) {
      return { error };
    }
  }
}
