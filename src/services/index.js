import DeviceInfo from 'react-native-device-info';
import APIService from './api';
import DBService from './db';


export default class Service {
  constructor() {
    this.APIService = new APIService();
    this.DBService = new DBService();
  }

  async login(password) {
    const deviceId = DeviceInfo.getUniqueID();
    const { info, error } = await this.APIService.login({ password, deviceId });
    this.DBService.insertUser(info);
    if (error) return { info, error };
    return { info };
  }

  async registerUser(userInfo) {
    try {
      const deviceId = DeviceInfo.getUniqueID();
      const extended = { ...userInfo, deviceId };
      const { info, error } = await this.APIService.registerUser(extended);
      if (error) return { info, error };
      info.deviceId = DeviceInfo.getUniqueID();
      info.password = extended.password;
      const dbResult = this.DBService.insertUser(info);
      if (dbResult.error) return dbResult.error;
      return { info };
    } catch (e) {
      return e;
    }
  }

  getUser() {
    try {
      const { userInfo, error } = this.DBService.retrieveUser();
      console.log(userInfo);
      console.log(error);
      return { userInfo, error };
    } catch (e) {
      return { error: e };
    }
  }

  async refreshToken(oldUserInfo) {
    try {
      const { userInfo, error } = await this.APIService.refreshToken(oldUserInfo);
      console.log('STEP');
      console.log(userInfo);
      this.DBService.updateUser(userInfo);
      return { userInfo, error };
    } catch (e) {
      return { error: e };
    }
  }

  async getEventTypeList() {
    try {
      const { userInfo, error } = this.getUser();
      if (error) return error;
      const { status, body } = await this.APIService.fetchEventTypes(userInfo);
      if (status === 401 && body.message === 'jwt expired') {
        await this.refreshToken(userInfo);
        // this.getEventTypeList();
      } else if (status !== 200) {
        return { error: body };
      }
      return { data: body };
    } catch (e) {
      return { erro: e };
    }
  }
}
